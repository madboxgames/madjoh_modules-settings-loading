# loading settings #

v1.0.1

Here are defined the methods to execute when the loading is complete and the user has no active session and the method to execute when the loader is incrementing. By default when the loader is incrementing, it increments a progress that should be placed in your HTML. Learn more in the loading_animation module documentation.